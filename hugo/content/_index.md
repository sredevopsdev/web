---
title: SREDevOps.dev
type: page
slug: inicio
date: 2023-01-14T04:28:38.461Z
draft: false
description: Comunidad SRE, DevOps y Platform Engineering en Castellano
lastmod: 2023-01-14T04:49:24.046Z
---
<p></p>
{{< image
src="logo-h.png"
align="center"
alt="SREDevOps.dev - Site Reliability Engineering, DevOps y Platform Engineering en Español" >}}

{{< center align="center" contenido="Qué es SREDevOps.dev?" >}}

__Quizás lo podemos describir de otra manera... intentemos.__

```bash
  #!/bin/env bash
  # Simple script que describe la comunidad SREDevOps.dev

  URL="https://sredevops.dev"
  NOMBRE="SREDevOps.dev"
  DESC="Comunidad SRE, DevOps y Platform Engineering en Castellano"
  echo "$NOMBRE - $DESC"
  
  # Fin del script
```
{{< center align="center" contenido="Qué queremos hacer?" >}}

```yaml
    ---
    # Archivo de configuración de la comunidad
    tipo: proyecto
    nombre: SREDevOps.dev
      queremos:
        construir:
          - Curiosidad Continua
          - Comunidades como Soporte
        para:
          data-propósito: | 
            Difundir las culturas, prácticas, roles,      
            tecnologías y noticias del universo cloud     
            native, desde la perspectiva de una comunidad 
            de personas con diversidad de experiencias,   
            culturas e identidades, que han desempeñando  
            roles como SREs, Platform Engineers, DevOps,  
            Developers, etc.'
        cómo:
          facilitando:
            - reunir-personas
            - facilitar-plataformas
          que-deseen:
            - aprender
            - enseñar
            - compartir
            - comunicar
            - enlazar
            - contribuir
    ---
    tipo: acciones-y-actividades
    nombre: propuestas-2023 
    metadatos:
      cómo:
        facilitar:
          - reunir-personas
          - plataformas
          - recursos-educativos
          - información relevante
        foco-en:
          - Latinoamérica
          - Español
    plataformas:
      - discord:
        enlace: 'https://discord.gg/8Z3q3Y2'
      - github:
        enlace: 'https://github.com/sredevopsdev'
      - instagram:
        enlace: 'https://www.instagram.com/sredevops/'

```

{{< discord "976621463874457621" "ngeorger" "80%" "380px" >}}
